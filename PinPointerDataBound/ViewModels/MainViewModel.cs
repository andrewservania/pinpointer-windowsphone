﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using PinPointerDataBound.Resources;

namespace PinPointerDataBound.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            this.Items = new ObservableCollection<ItemViewModel>();
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<ItemViewModel> Items { get; private set; }

        private string _sampleProperty = "Sample Runtime Property Value";
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>
        public string SampleProperty
        {
            get
            {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        /// <summary>
        /// Sample property that returns a localized string
        /// </summary>
        public string LocalizedSampleProperty
        {
            get
            {
                return AppResources.SampleProperty;
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            Random random = new Random();
            
            // Sample data; replace with real data
            this.Items.Add(new ItemViewModel() { ID = "0", LineOne = "freelancer  one", LineTwo = "Cake Baker", LineThree = "Facilisi faucibus habitant inceptos interdum lobortis nascetur pharetra placerat pulvinar sagittis senectus sociosqu", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "1", LineOne = "freelancer  two", LineTwo = "Bike fixer", LineThree = "Suscipit torquent ultrices vehicula volutpat maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "2", LineOne = "freelancer  three", LineTwo = "Plumber", LineThree = "Habitant inceptos interdum lobortis nascetur pharetra placerat pulvinar sagittis senectus sociosqu suscipit torquent", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "3", LineOne = "freelancer  four", LineTwo = "Taxi driver", LineThree = "Ultrices vehicula volutpat maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus habitant inceptos", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "4", LineOne = "freelancer  five", LineTwo = "Print your stuff 24/7", LineThree = "Maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus habitant inceptos interdum lobortis nascetur", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "5", LineOne = "freelancer  six", LineTwo = "Masseuse", LineThree = "Pharetra placerat pulvinar sagittis senectus sociosqu suscipit torquent ultrices vehicula volutpat maecenas praesent", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "6", LineOne = "company seven", LineTwo = "Party clown", LineThree = "Accumsan bibendum dictumst eleifend facilisi faucibus habitant inceptos interdum lobortis nascetur pharetra placerat", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "7", LineOne = "company eight", LineTwo = "Mover", LineThree = "Pulvinar sagittis senectus sociosqu suscipit torquent ultrices vehicula volutpat maecenas praesent accumsan bibendum", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "8", LineOne = "company nine", LineTwo = "Carpenter", LineThree = "Facilisi faucibus habitant inceptos interdum lobortis nascetur pharetra placerat pulvinar sagittis senectus sociosqu", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "9", LineOne = "freelancer ten", LineTwo = "Make up artist", LineThree = "Suscipit torquent ultrices vehicula volutpat maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "10", LineOne = "freelancer eleven", LineTwo = "Sculpture artist", LineThree = "Habitant inceptos interdum lobortis nascetur pharetra placerat pulvinar sagittis senectus sociosqu suscipit torquent", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "11", LineOne = "freelancer twelve", LineTwo = "Computer repair guy", LineThree = "Ultrices vehicula volutpat maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus habitant inceptos", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "12", LineOne = "freelancer thirteen", LineTwo = "Mechanic", LineThree = "Maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus habitant inceptos interdum lobortis nascetur", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "13", LineOne = "freelancer fourteen", LineTwo = "Tiler", LineThree = "Pharetra placerat pulvinar sagittis senectus sociosqu suscipit torquent ultrices vehicula volutpat maecenas praesent", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "14", LineOne = "freelancer fifteen", LineTwo = "Pastry baker", LineThree = "Accumsan bibendum dictumst eleifend facilisi faucibus habitant inceptos interdum lobortis nascetur pharetra placerat", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });
            this.Items.Add(new ItemViewModel() { ID = "15", LineOne = "freelancer sixteen", LineTwo = "Chef", LineThree = "Pulvinar sagittis senectus sociosqu suscipit torquent ultrices vehicula volutpat maecenas praesent accumsan bibendum", Picture = "Assets/ServiceProvider"+ (1 + random.Next(5))+ ".png" });

            this.IsDataLoaded = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}