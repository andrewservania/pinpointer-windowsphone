﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinPointer
{
    /// <summary>
    /// A class that serves as a data model for items that are to
    /// be displayed on screen
    /// </summary>
    public class ServiceProvider
    {
        // The name of the service provider
        protected string name { get; set; }

        // A quick description of the service provider
        protected string quickDescription {get; set;}

        // A small picture of the service provider. 
        // Small enough to fit in a list view
       // protected Bitmap picture { get; set; }

    }
}
